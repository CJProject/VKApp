//
//  Completion.swift
//  gosuslugispb
//
//  Created by Aleksey Sigay on 18.12.2019.
//

import Foundation

typealias Completion = () -> Void
