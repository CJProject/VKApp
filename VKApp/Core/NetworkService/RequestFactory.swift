//
//  RequestFactory.swift
//  VKApp
//
//  Created by Alexey Sigay on 30.11.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import Foundation

/// The factory provides creating an URLRequest according to the
/// [VK API](https://dev.vk.com/method)

struct RequestFactory {
    
    // MARK: - Properties
    
    /// AccessToken that was passed during the authorisation
    let accessToken: String
    /// VK API version
    let apiVersion: String
    
    // MARK: - Functions
    
    /// Creating an URLRequest according to the VK API from NetworkRequest protocol
    func create(_ request: NetworkRequest) -> URLRequest? {
        guard let url = url(for: request) else { return nil }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = request.method?.rawValue ?? HTTPMethod.get.rawValue
        
        return urlRequest
    }
    
    // MARK: - Private functions
    
    private func url(for request: NetworkRequest) -> URL? {
        var components = URLComponents()
        components.scheme = request.scheme?.rawValue ?? URLScheme.https.rawValue
        components.host = request.host?.rawValue ?? URLHost.productionServer.rawValue
        components.path = request.path
        
        components.queryItems = [
            URLQueryItem(name: "access_token", value: accessToken),
            URLQueryItem(name: "v", value: apiVersion)
        ]

        guard let parameters = request.parameters else { return components.url }

        for parameter in parameters {
            guard let value = stringValue(from: parameter.value) else {
                continue
            }
            
            components.queryItems?.append(
                URLQueryItem(name: parameter.key, value: value)
            )
        }
        
        return components.url
    }
    
    private func stringValue(from value: Any) -> String? {
        if let stringValue = value as? String {
            return stringValue
        }
        
        if let intValue = value as? Int {
            return String(intValue)
        }
        
        return nil
    }
}
