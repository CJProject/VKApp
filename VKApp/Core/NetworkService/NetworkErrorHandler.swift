//
//  NetworkErrorHandler.swift
//  VKApp
//
//  Created by Alexey Sigay on 09.12.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import Foundation

/// Network errors handler

protocol NetworkErrorHandling {
    func handleDecodingError<T: Decodable>(_ error: DecodingError, for: T.Type)
}

struct NetworkErrorHandler: NetworkErrorHandling {
    
    /// DecodingError handling and with displaying details
    func handleDecodingError<T: Decodable>(_ error: DecodingError, for: T.Type) {
        NSLog("NetworkErrorHandler: \(T.self) decoding error")
        switch error {
        case .typeMismatch(let key, let value):
            NSLog("\(error.localizedDescription) \(key), value \(value)")
        case .valueNotFound(let key, let value):
            NSLog("\(error.localizedDescription) \(key), value \(value)")
        case .keyNotFound(let key, let value):
            NSLog("\(error.localizedDescription) \(key), value \(value)")
        case .dataCorrupted(let key):
            NSLog("\(error.localizedDescription) \(key)")
        default:
            NSLog("\(error.localizedDescription)")
        }
    }
}
