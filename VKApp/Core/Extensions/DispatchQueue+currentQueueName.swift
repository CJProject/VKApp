//
//  DispatchQueue+currentQueueName.swift
//  VKApp
//
//  Created by Alexey Sigay on 10.12.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import Foundation

extension DispatchQueue {
    
    /// The function simplifies getting the name of a queue and its QoS
    static func currentQueueName() -> String {
        let name = __dispatch_queue_get_label(nil)
        return String(cString: name, encoding: .utf8) ?? "unknown"
    }
}
