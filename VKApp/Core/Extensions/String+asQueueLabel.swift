//
//  String+asQueueLabel.swift
//  VKApp
//
//  Created by Alexey Sigay on 10.12.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import Foundation

extension String {
    
    /// The function adds unique project prefix to a queue label
    func asQueueLabel() -> String {
        "VKApp." + self
    }
}
