//
//  Constant.swift
//  VKApp
//
//  Created by Алексей Сигай on 22/06/2019.
//  Copyright © 2019 CJProject. All rights reserved.
//

import UIKit

struct Constant {
    static let vkApiVersion =  "5.95"
    static let vkAppID =       "6360164"
}
