//
//  UITextFieldWithHint.swift
//  TestTextFieldVSHint
//
//  Created by Алексей Сигай on 05/06/2019.
//  Copyright © 2019 Алексей Сигай. All rights reserved.
//

import UIKit

/**
 UITextField с всплывающим Placeholder
 
 При вводе текста Placeholder выходит за границы TextField и остается читаемым как для видящих,
 так и для слабовидящих посредством VoiceOver.
 
 - Author: Sigay Aleksey
 
 - Version: 1.0
 
 - Parameters:
 - PlaceholderColor: цвет Placeholder в рамках TextField;
 - ErrorTextColor: цвет текста в TextField, Placeholder или поднятого Placeholder при вызове метода setErrorStyle();
 - MainTextColor: цвет текста в TextField и поднятого Placeholder в нормальном состоянии.

        ┌──────────────────────┐
        │    ↕ 34  TextField   │
        └──────────────────────┘
            Text Size = 17
 */

@IBDesignable
final class UITextFieldWithHint: UITextField {

    // MARK: - IBInspectable
    
    // Цветовая гамма для TextField
    @IBInspectable var PlaceholderColor: UIColor = UIColor(red: 200/255, green: 200/255, blue: 200/255, alpha: 1)
    @IBInspectable var ErrorTextColor: UIColor = UIColor.red
    @IBInspectable var MainTextColor: UIColor = UIColor.black
    
    // Включение возможности вставки текста в TextField
    @IBInspectable var PasteEnable: Bool = true
    
    
    // MARK: - General Setting
    
    // Время анимации подъема HintLabel
    private let animationDuration = 0.4

    private lazy var textFieldHeight = self.frame.height
    private var isFirstStart = true
    
    // Свойства HintLabel
    private var hintLabel = UILabel()
    private let hintLabelLeftIndent: CGFloat = 8
    private lazy var hintLabelHeight = textFieldHeight - 2
    private lazy var hintLabelHalbHeight = hintLabelHeight/2
    private lazy var heightOfLift = hintLabelHeight - 4
    private var showLabelHideTransform = CGAffineTransform.identity
    
    // Позиция HintLabel в сскрытом состоянии
    private lazy var hidePosition = CGRect(x: self.frame.minX + hintLabelLeftIndent,
                                           y: self.frame.minY + 1,
                                           width: self.frame.width - self.frame.minX - (self.frame.maxX - UIScreen.main.bounds.width) - hintLabelLeftIndent,
                                           height: textFieldHeight - 2)
    

    // MARK: - Init
    override func layoutSubviews() {
        super.layoutSubviews()
        if isFirstStart {
            setup()
            isFirstStart = false
        }
    }
    
    private func setup() {
        configureTransform()
        configureTextField(externalHeight: frame.height)
        configureHintLabel(externalHeight: frame.height, sender: self)
    }
    
    
    // MARK: - Error indication
    
    public func setErrorStyle() {
        // Приведение цветовой гаммы в состояние ошибки
        textColor = ErrorTextColor
        self.setValue(ErrorTextColor, forKeyPath: "placeholderLabel.textColor")
        hintLabel.textColor = ErrorTextColor
    }
    
    public func setErrorStyle(errorMessage: String) {
        hintLabel.text = "\(placeholder!). \(errorMessage)"
        
        // Приведение цветовой гаммы в состояние ошибки
        textColor = ErrorTextColor
        self.setValue(ErrorTextColor, forKeyPath: "placeholderLabel.textColor")
        hintLabel.textColor = ErrorTextColor
    }
    
    
    // MARK: - Initial Configuration
    
    /// Конфигурация
    private func configureTransform() {
        showLabelHideTransform = showLabelHideTransform.scaledBy(x: 0.8, y: 0.8)
        showLabelHideTransform = showLabelHideTransform.translatedBy(x: -textFieldHeight, y: -textFieldHeight)
    }
    
    /// Конфигурация TextField
    private func configureTextField(externalHeight: CGFloat) {
        // Действие при изменении текстового поля
        self.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
    }
    
    /// Стартовая конфигурация HintLabel
    private func configureHintLabel(externalHeight: CGFloat, sender: UITextField) {
        if self.isHidden == false {
            sender.superview!.addSubview(hintLabel)
            hintLabel.text = placeholder
            hintLabel.font = UIFont.systemFont(ofSize: self.font!.pointSize)
            hintLabel.frame = hidePosition
            
            if self.text!.isEmpty {
                hintLabel.textColor = PlaceholderColor
                hintLabel.isHidden = true
            } else {
                hintLabel.transform = showLabelHideTransform
                hintLabel.textColor = MainTextColor
                hintLabel.isHidden = false
            }
        }
    }
    
    /// Настройка возможности вставки текста в UITextField
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if !PasteEnable {
            if action == #selector(UIResponderStandardEditActions.paste(_:)) {
                return false
            }
            return super.canPerformAction(action, withSender: sender)
        } else {
            return super.canPerformAction(action, withSender: sender)
        }
    }

    
    // MARK: - TextField Delegate
    
    // Действия при редактировании UITextField
    @objc func textFieldDidChange() {
        hintLabel.text = placeholder
        // Приведение цветовой гаммы в нормальное состояние
        self.textColor = MainTextColor
        self.setValue(PlaceholderColor, forKeyPath: "placeholderLabel.textColor")
        hintLabel.textColor = MainTextColor
        
        moveHintLabel()
    }
    
    
    // MARK: - HintLabel Driver
    
    // Скрытие HintLabel при нажатии на clearButton
    internal func textFieldShouldClear(_ textField: UITextField) -> Bool {
        dropHintLabel()
        return true
    }

    /// Управление положением HintLabel
    private func moveHintLabel() {
        if hintLabel.frame.minY > 0 {
            liftHintLabel()
        }
        if self.text!.count == 0 {
            dropHintLabel()
        }
    }

    /// Подъем HintLabel
    private func liftHintLabel() {
        hintLabel.textColor = PlaceholderColor
        hintLabel.isHidden = false
        // Анимация цвета и frame
        UIView.transition(with: hintLabel, duration: animationDuration, options: .transitionCrossDissolve, animations: {
            self.hintLabel.textColor = self.MainTextColor
            self.hintLabel.transform = self.showLabelHideTransform
        }, completion: nil)
    }

    /// Скрытие HintLabel
    private func dropHintLabel() {
        // Анимация цвета и frame
        UIView.transition(with: hintLabel, duration: 0.1, options: .transitionCrossDissolve, animations: {
            self.hintLabel.textColor = self.PlaceholderColor
            self.hintLabel.transform = .identity
        }, completion: nil)
        hintLabel.isHidden = true
    }
}
