//
//  ErrorView.swift
//  VKApp
//
//  Created by Alexey Sigay on 19.12.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import SwiftUI

/// View for dispaly an error for user

struct ErrorView: View {
    
    // MARK: - Properties
    
    let text: String
    let delegate: ErrorViewDelegate?
    
    // MARK: - View body
    
    var body: some View {
        VStack(spacing: 20) {
            Text(text)
            
            if let delegate = delegate,
               delegate.repeatableOptions == .repeatable {
                HStack(spacing: 20) {
                    Button("Cancel", action: delegate.cancelAction)
                    Button("Repeat", action: delegate.repeatAction)
                }
                .buttonStyle(.bordered)
            }
            if let delegate = delegate,
               delegate.repeatableOptions == .noRepeatOption {
                HStack(spacing: 20) {
                    Button("Cancel", action: delegate.cancelAction)
                }
                .buttonStyle(.bordered)
            }
        }
    }
}

struct ErrorView_Previews: PreviewProvider {
    static var previews: some View {
        ErrorView(
            text: NetworkError.serverError.description,
            delegate: StubErrorViewDelegate()
        )
    }
}
