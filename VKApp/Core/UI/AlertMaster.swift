//
//  AlertMaster.swift
//  VKApp
//
//  Created by Алексей Сигай on 18/06/2019.
//  Copyright © 2019 CJProject. All rights reserved.
//

import UIKit


/**
 Class to display alerts to the user
 */
final class AlertMaster {
    
    typealias MethodHandler = () -> Void

    /**
     Alert with one button OK
     */
    func showAlertOK(title: String, message: String?,
                     controller: UIViewController,
                     action: MethodHandler? = nil) {
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ОК", style: .default, handler: { (alert) in
            if action != nil { action!() }
        }))
        DispatchQueue.main.async {
            controller.present(alert, animated: true, completion: nil)
        }
    }
    
    /**
     Alert with buttons YES / NO
     */
    func showAlertYesNo(title: String, message: String,
                        controller: UIViewController,
                        actionForYes: MethodHandler?,
                        actionForNo: MethodHandler? = nil) {
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Нет", style: .cancel, handler: { (alert) in
            if actionForNo != nil { actionForNo!() }
        }))
        alert.addAction(UIAlertAction(title: "Да", style: .default, handler: { (alert) in
            if actionForYes != nil { actionForYes!() }
        }))
        controller.present(alert, animated: true, completion: nil)
    }
}
