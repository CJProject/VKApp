//
//  AdvancedAsyncImage.swift
//  VKApp
//
//  Created by Alexey Sigay on 16.12.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import SwiftUI

/// Wrapper over standard AsyncImage
///
/// It allows to use delegate for error handling

struct AdvancedAsyncImage<Content>: View where Content: View {
    
    // MARK: - Private properties
    
    private let url: URL?
    private let delegate: AdvancedAsyncImageDelegate?
    private var content: (_ image: Image) -> Content
    
    private var errorImage: some View  {
        Image(systemName: "photo")
            .imageScale(.large)
            .foregroundColor(.gray)
    }

    // MARK: - Init
    
    public init(
        url: URL?,
        delegate: AdvancedAsyncImageDelegate? = nil,
        @ViewBuilder content: @escaping (_ image: Image) -> Content
    ) {
        self.url = url
        self.delegate = delegate
        self.content = content
    }

    // MARK: - View body
    
    var body: some View {
        AsyncImage(
            url: url,
            content: { phase in
                switch phase {
                case .empty:
                    ProgressView()
                case .success(let image):
                    image
                        .resizable()
                case .failure(let error):
                    errorView(with: error)
                @unknown default:
                    errorView()
                }
            }
        )
    }
    
    // MARK: - Private functions
    
    private func errorView() -> some View {
        errorImage
    }
    
    private func errorView(with error: Error) -> some View {
        delegate?.imageLoadingError(error, withURL: url)
        return errorImage
    }
}


/// Delegate protocol for error handling when AsyncImage uploading images
///
/// It allows to process an error reason and image URL
protocol AdvancedAsyncImageDelegate {
    /// Returning a loading image error with its URL
    func imageLoadingError(_ error: Error, withURL url: URL?)
}
