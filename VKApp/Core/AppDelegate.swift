//
//  AppDelegate.swift
//  VKApp
//
//  Created by Алексей Сигай on 18/06/2019.
//  Copyright © 2019 CJProject. All rights reserved.
//

import UIKit
import VK_ios_sdk

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var authorizationMaster: AuthorizationMaster?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        setupURLCache()
        
        // AuthorizationMaster initialization for choosing rootViewController
        authorizationMaster = AuthorizationMaster()

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        window?.rootViewController = authorizationMaster?.rootViewController()
        
        return true
    }
    
    /// VKAuthorization method
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        VKSdk.processOpen(url, fromApplication: UIApplication.OpenURLOptionsKey.sourceApplication.rawValue)
        return true
    }
}

private extension AppDelegate {
    func setupURLCache() {
        URLCache.shared.memoryCapacity = 10_000_000 // ~10 MB
        URLCache.shared.diskCapacity = 100_000_000 // ~100 MB
    }
}
