//
//  State.swift
//  VKApp
//
//  Created by Alexey Sigay on 12.12.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import Foundation

/// Displays the status of receiving a data from the network

enum State<T> {
    case none
    case loading
    case success(result: T)
    case error(error: NetworkError)
}
