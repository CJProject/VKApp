//
//  DateConverter.swift
//  VKApp
//
//  Created by Alexey Sigay on 13.12.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import Foundation

/// Converter of Date to different formats
protocol DateConvering {
    /// Getting date from Int
    func formatedDate(from date: Int) -> String
}

struct DateConverter: DateConvering {
    
    // MARK: - Functions
    
    func formatedDate(from date: Int) -> String {
        let dateAsDate = Date(timeIntervalSince1970: Double(date))
        return dateFormatter.string(from: dateAsDate)
    }
    
    // MARK: - Private properties
    
    /// Date style: January 31, 2023
    private let dateFormatter: DateFormatter = {
        let formater = DateFormatter()
        formater.timeZone = TimeZone.current
        formater.dateFormat = "MMMM dd, yyyy"
        return formater
    }()
}
