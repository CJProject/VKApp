//
//  MainTabBarController.swift
//  VKApp
//
//  Created by Alexey Sigay on 18/06/2019.
//  Copyright © 2019 CJProject. All rights reserved.
//

import UIKit
import SwiftUI

/// TabBarController with app content which is displayed for an authorized user

final class MainTabBarController: UITabBarController {

    // MARK: - UIViewController life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewControllers = createViewControllers()
    }
    
    // MARK: - Private functions
    
    private func createViewControllers() -> [UIViewController] {
        return [
            createNewsViewController()
        ]
    }
    
    private func createNewsViewController() -> UIViewController {
        let title = "News"
        let viewController = UIHostingController(rootView: NewsFactory.cteate())
        viewController.navigationItem.title = title
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.tabBarItem.title = title
        navigationController.tabBarItem.image = UIImage(named: "message")

        return navigationController
    }
}
