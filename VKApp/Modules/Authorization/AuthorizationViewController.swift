//
//  AuthorizationViewController.swift
//  VKApp
//
//  Created by Alexey Sigay on 18/06/2019.
//  Copyright © 2019 CJProject. All rights reserved.
//

import UIKit
import VK_ios_sdk

/// The class responsible for user authorization
///
/**
 During authorization, the user enters the VK Web page for secure login and password entry.
 In case of successful authorization, the application receives a token.
 */

final class AuthorizationViewController: UIViewController {
    
    // MARK: - Properties
    
    let alertMaster = AlertMaster()
    
    // MARK: - Private properties
    
    private let scope = [VK_PER_FRIENDS, VK_PER_GROUPS, VK_PER_WALL]
    private let userDefaults = UserDefaults.standard
    private let keyVKToken = "keyVKToken"
    
    // MARK: - UIViewController life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        wakeUpSession()
    }
    
    // MARK: - Private functions
    
    private func setupView() {
        let authorizationView = AuthorizationView()
        authorizationView.entranceButton.addTarget(self, action: #selector(logIn), for: .touchUpInside)
        view = authorizationView
    }
    
    /// Checking the previous session availability
    private func wakeUpSession() {
        VKSdk.initialize(withAppId: Constant.vkAppID, apiVersion: Constant.vkApiVersion).register(self)
        VKSdk.instance().uiDelegate = self
        
        VKSdk.wakeUpSession(scope) { state, error in
            if state == .authorized {
                NSLog("Authorization: Authorized")
                self.entryIntoTheApp()
            } else if state == .initialized {
                NSLog("Authorization: Initialized")
            } else if state == .error {
                NSLog("Authorization: Error")
                let title = "Ошибка входа"
                let message = "Попробуте еще раз"
                self.alertMaster.showAlertOK(title: title, message: message, controller: self, action: nil)
            }
        }
    }
    
    /// Authorization start
    @objc private func logIn() {
        VKSdk.forceLogout()
        if VKSdk.vkAppMayExists() == true {
            VKSdk.authorize(scope, with: .unlimitedToken)
        } else {
            VKSdk.authorize(scope, with: [.disableSafariController, .unlimitedToken])
        }
    }
    
    /// Getting VK UserId
    private func getUserId() -> String {
        return String(VKSdk.accessToken().userId)
    }
    
    /// Getting VK Token
    private func getToken() -> String {
        return String(VKSdk.accessToken().accessToken)
    }
    

    // MARK: - Navigation

    /// Transition to the MainTabBarController
    private func entryIntoTheApp() {
        self.navigationController?.pushViewController(MainTabBarController(), animated: true)
    }
}


extension AuthorizationViewController: VKSdkDelegate, VKSdkUIDelegate {
    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        guard let token = result.token
            else {
                NSLog("Authorization: Finished without authorization")
                let title = "Ошибка авторизации"
                let message = "Попробуте еще раз"
                self.alertMaster.showAlertOK(title: title, message: message, controller: self, action: nil)
                return
        }
        NetworkService.accessToken = token.accessToken
        print(token.accessToken!)
        entryIntoTheApp()
    }
    
    func vkSdkUserAuthorizationFailed() {
        NSLog("Authorization: Failed")
        self.dismiss(animated: true, completion: nil)
    }
    
    /// Authorization using Safari
    func vkSdkShouldPresent(_ controller: UIViewController!) {
        self.navigationController?.topViewController?.present(controller, animated: true, completion: nil)
    }
    
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        var viewController: VKCaptchaViewController
        viewController = VKCaptchaViewController.captchaControllerWithError(captchaError)
        viewController.present(in: self.navigationController?.topViewController)
    }
}
