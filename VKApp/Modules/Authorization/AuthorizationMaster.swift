//
//  AuthorizationMaster.swift
//  VKApp
//
//  Created by Alexey Sigay on 22/06/2019.
//  Copyright © 2019 CJProject. All rights reserved.
//

import Foundation
import VK_ios_sdk

/// Authorization check
/**
 - If the current user was authorized, the transition to the News screen is performed
 - If the current user was not authorized, the transition to the Authorization screen is performed
 */
final class AuthorizationMaster: NSObject {

    /// Tocken access level
    private let scope = [VK_PER_FRIENDS, VK_PER_GROUPS, VK_PER_WALL]

    /// ViewController depending on authorization status
    func rootViewController() -> UIViewController {
        VKSdk.initialize(withAppId: Constant.vkAppID, apiVersion: Constant.vkApiVersion).register(self)
        VKSdk.instance().uiDelegate = self
        
        // Running a required method to check authorization
        VKSdk.wakeUpSession(scope) { state, error in
            if state == .authorized {
                NSLog("AuthorizationMaster: AuthorizationStatus is authorized")
            } else {
                NSLog("AuthorizationMaster: AuthorizationStatus is not authorized")
            }
        }
        
        // Chech Authorization
        if VKSdk.isLoggedIn() {
            NetworkService.accessToken = VKSdk.accessToken().accessToken
            NSLog("AuthorizationMaster token: ", NetworkService.accessToken)
            return MainTabBarController()
        } else {
            return AuthorizationViewController()
        }
    }
}


extension AuthorizationMaster: VKSdkDelegate, VKSdkUIDelegate {
    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
    }

    func vkSdkUserAuthorizationFailed() {
    }

    func vkSdkShouldPresent(_ controller: UIViewController!) {
    }

    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
    }
}
