//
//  AuthorizationView.swift
//  VKApp
//
//  Created by Alexey Sigay on 20.12.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import UIKit

/// View to display for as unauthorized user

final class AuthorizationView: UIView {
    
    // MARK: - Properties
    
    let titleLabel = UILabel()
    let entranceButton = UIButton(type: .system)
    
    // MARK: - UIView functions
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setup()
    }
    
    // MARK: - Private functions

    private func setup() {
        titleLabel.text = "VKApp"
        titleLabel.font = UIFont(name: "Marker Felt Thin", size: 46)
        
        entranceButton.setTitle("Log In", for: .normal)
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 150).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        entranceButton.translatesAutoresizingMaskIntoConstraints = false
        entranceButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -150).isActive = true
        entranceButton.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
}
