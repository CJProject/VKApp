//
//  NewsFactory.swift
//  VKApp
//
//  Created by Alexey Sigay on 20.12.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import SwiftUI

/// Factory of News feature

struct NewsFactory {
    static func cteate() -> some View {
        let dataStore = NewsDataStore()
        let responseTransformer = NewsResponseTransformer(
            dateConverter: DateConverter(),
            dataStore: dataStore
        )
        let viewModel =  NewsViewModel(
            dataStore: dataStore,
            networkService: NetworkService.shared,
            responseTransformer: responseTransformer
        )
        
        return NewsView(viewModel: viewModel)
    }
}
