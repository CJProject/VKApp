//
//  NewsItem.swift
//  VKApp
//
//  Created by Alexey Sigay on 16.12.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import Foundation

/// New data in case of its type

enum NewsItem {
    case post(item: NewsPostItem)
    case photo(item: NewsPhotoItem)
    case wallPhoto(item: NewsWallPhotoItem)
}

extension NewsItem: Identifiable {
    var id: UUID {
        return UUID()
    }
}


/// New's type according to VK API

enum NewsItemType: String {
    case post = "post"
    case photo = "photo"
    case wallPhoto = "wall_photo"
    case unknown
}
