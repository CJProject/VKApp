//
//  NewsPostItem.swift
//  VKApp
//
//  Created by Alexey Sigay on 12.12.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import Foundation

/// New of post type

struct NewsPostItem: Codable, Identifiable {
    let id: Int
    let source: NewsSource
    let text: String
    let imageURL: String
    let imageSize: CGSize
    let feedback: NewsFeedback
}

extension NewsPostItem {
    struct Stub {
        static let data = NewsPostItem(
            id: 777,
            source: NewsSource.Stub.data,
            text: "New some text",
            imageURL: "mountain",
            imageSize: CGSize(width: 300, height: 200),
            feedback: NewsFeedback.Stub.data
        )
    }
}

extension NewsPostItem: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
