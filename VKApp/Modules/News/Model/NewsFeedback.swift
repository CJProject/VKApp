//
//  NewsFeedback.swift
//  VKApp
//
//  Created by Alexey Sigay on 12.12.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import Foundation

/// Data source for NewsFeedbackView displaying

struct NewsFeedback: Hashable, Codable {
    let like: NewsFeedbackItem
    let comment: NewsFeedbackItem
    let repost: NewsFeedbackItem
    let view: NewsFeedbackItem
}

extension NewsFeedback {
    struct Stub {
        static let data = NewsFeedback(
            like: NewsFeedbackItem.Stub.data,
            comment: NewsFeedbackItem.Stub.data,
            repost: NewsFeedbackItem.Stub.data,
            view: NewsFeedbackItem.Stub.data
        )
    }
}
