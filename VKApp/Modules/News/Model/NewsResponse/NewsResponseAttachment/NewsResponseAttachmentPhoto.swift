//
//  NewsResponseAttachmentPhoto.swift
//  VKApp
//
//  Created by Alexey Sigay on 14/09/2019.
//  Copyright © 2019 CJProject. All rights reserved.
//

import Foundation

/**
 A part of structure NewsResponse
 
 It includes one photo data
 */

struct NewsResponseAttachmentPhoto: Decodable {
    let id: Int
    let albumID: Int
    let ownerID: Int
    let userID: Int?
    let sizes: [NewsResponsePhotoSize]
    let text: String?
    let date: Int
    let accessKey: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case albumID = "album_id"
        case ownerID = "owner_id"
        case userID = "user_id"
        case sizes, text, date
        case accessKey = "access_key"
    }
}
