//
//  NewsResponseAttachmentDoc.swift
//  VKApp
//
//  Created by Alexey Sigay on 14/09/2019.
//  Copyright © 2019 CJProject. All rights reserved.
//

import Foundation

/**
 A part of structure NewsResponse
 
 It includes one document data
 */

struct NewsResponseAttachmentDoc: Decodable {
    let id: Int
    let ownerID: Int
    let title: String
    let size: Int
    let ext: String     // gif
    let url: String
    let date: Int
    let type: Int
    let preview: Preview
    let accessKey: String
    
    struct Preview: Decodable {
        let photo: Photo
        let video: Video
        
        struct Photo: Decodable {
            let sizes: [Size]
            
            struct Size: Decodable {
                let src: String
                let width: Int
                let height: Int
                let type: String    // m / s / x / o
            }
        }
        
        struct Video: Decodable {
            let src: String
            let width: Int
            let height: Int
            let fileSize: Int
            
            enum CodingKeys: String, CodingKey {
                case src, width, height
                case fileSize = "file_size"
            }
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case ownerID = "owner_id"
        case title, size, ext, url, date, type, preview
        case accessKey = "access_key"
    }
}
