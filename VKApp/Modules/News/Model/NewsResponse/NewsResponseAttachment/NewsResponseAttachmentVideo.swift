//
//  NewsResponseAttachmentVideo.swift
//  VKApp
//
//  Created by Alexey Sigay on 14/09/2019.
//  Copyright © 2019 CJProject. All rights reserved.
//

import Foundation

/**
 A part of structure NewsResponse
 
 It includes one video data
 */

struct NewsResponseAttachmentVideo: Decodable {
    let id: Int
    let ownerID: Int
    let title: String
    let duration: Int
    let date: Int
    let comments: Int?
    let views: Int
    let localViews: Int?
    let width: Int?
    let height: Int?
    let photo130: String
    let photo320: String
    let photo640: String?
    let photo800: String
    let photo1280: String?
    let firstFrame130: String?
    let firstFrame160: String?
    let firstFrame320: String?
    let firstFrame720: String?
    let firstFrame800: String?
    let firstFrame1024: String?
    let firstFrame1280: String?
    let firstFrame4096: String?
    let isFavorite: Bool
    let platform: String?    // YouTube
    let repeatKey: Int? // del
    let canAdd: Int
    let trackCode: String
    
    
    enum CodingKeys: String, CodingKey {
        case id
        case ownerID = "owner_id"
        case title, duration, date, comments, views, width, height
        case localViews = "local_views"
        case photo130 = "photo_130"
        case photo320 = "photo_320"
        case photo640 = "photo_640"
        case photo800 = "photo_800"
        case photo1280 = "photo_1280"
        case firstFrame130 = "first_frame_130"
        case firstFrame160 = "first_frame_160"
        case firstFrame320 = "first_frame_320"
        case firstFrame720 = "first_frame_720"
        case firstFrame800 = "first_frame_800"
        case firstFrame1024 = "first_frame_1024"
        case firstFrame1280 = "first_frame_1280"
        case firstFrame4096 = "first_frame_4096"
        case isFavorite = "is_favorite"
        case platform
        case repeatKey = "repeat"
        case canAdd = "can_add"
        case trackCode = "track_code"
    }
}
