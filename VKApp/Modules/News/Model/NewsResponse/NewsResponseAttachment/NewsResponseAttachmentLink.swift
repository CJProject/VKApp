//
//  NewsResponseAttachmentLink.swift
//  VKApp
//
//  Created by Alexey Sigay on 14/09/2019.
//  Copyright © 2019 CJProject. All rights reserved.
//

import Foundation

/**
 A part of structure NewsResponse
 */

struct NewsResponseAttachmentLink: Decodable {
    let url: String
    let title: String
    let caption: String?
    let description: String
    let target: String? // internal
    let photo: Photo?
    let button: Button?
    let is_favorite: Bool?
    
    struct Photo: Decodable {
        let id: Int
        let albumID: Int
        let ownerID: Int
        let userID: Int?
        let sizes: [NewsResponsePhotoSize]
        let text: String?
        let date: Int
        
        enum CodingKeys: String, CodingKey {
            case id
            case albumID = "album_id"
            case ownerID = "owner_id"
            case userID = "user_id"
            case sizes, text, date
        }
    }
    
    struct Button: Decodable {
        let title: String
        let action: Action
        
        struct Action: Decodable {
            let type: String
            let url: String
        }
    }
}
