//
//  NewsResponsePhotoSizes.swift
//  VKApp
//
//  Created by Alexey Sigay on 14/09/2019.
//  Copyright © 2019 CJProject. All rights reserved.
//

import Foundation

/**
 A part of structure NewsResponse
 
 It includes data for one document
 */

struct NewsResponsePhotoSize: Decodable {
    
    /**
     Data for one photo downloading
     
     * m - 130
     * o - 130
     * p - 200
     * q - 320
     * r - 510
     * s - 75
     * x - 604
     * y - 750
     */
    let type: String
    
    let url: String
    let width: Int
    let height: Int
}
