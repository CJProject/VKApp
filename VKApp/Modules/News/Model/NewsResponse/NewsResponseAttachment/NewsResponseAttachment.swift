//
//  NewsResponseAttachment.swift
//  VKApp
//
//  Created by Alexey Sigay on 14/09/2019.
//  Copyright © 2019 CJProject. All rights reserved.
//

import Foundation

/**
 A part of structure NewsResponse
 
 It includes one NewsResponseItem data
 (one photo, one video etc)
 */

struct NewsResponseAttachment: Decodable {
    let type: String    // post / photo / video / doc / poll / link / audio
    let photo: NewsResponseAttachmentPhoto?
    let video: NewsResponseAttachmentVideo?
    let doc: NewsResponseAttachmentDoc?
    let link: NewsResponseAttachmentLink?
}


/**
 Data type in NewsResponseItem
 
 In one attachment block could be several different data type
 */

enum NewsResponseAttachmentType: String {
    case post, photo, video, doc, poll, link, audio
}
