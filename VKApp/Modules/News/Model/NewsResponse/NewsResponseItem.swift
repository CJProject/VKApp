//
//  NewsResponseItem.swift
//  VKApp
//
//  Created by Alexey Sigay on 14/09/2019.
//  Copyright © 2019 CJProject. All rights reserved.
//

import Foundation

/**
 A part of structure NewsResponse
 
 It includes data about one new and includes:
 - new type;
 - new body attachments;
 - users reactions for the new.
 */

struct NewsResponseItem: Decodable {
    let type: String    // post / photo / wall_photo
    
    // The source of new. It needs to be found in profiles: NewsResponseProfile or in groups: NewsResponseGroup
    let sourceID: Int
    let date: Int
    let postID: Int
    
    // != nil only for wall_photo type
    let photos: Photos?
    
    
    // all of the following = nil for wall_photo type
    
    let postType: String?
    let text: String?
    
    // nil for post type
    let copyHistory: [CopyHistory]?
    
    let markedAsAds: Int?
    
    // nil for photo type
    let attachments: [NewsResponseAttachment]?
    
    let postSource: PostSource?
    let comments: NewReaction?
    let likes: NewReaction?
    let reposts: NewReaction?
    let views: NewReaction?
    let isFavorite: Bool?
    
    
    struct CopyHistory: Decodable {
        let id: Int
        let ownerID: Int
        let fromID: Int
        let date: Int
        let postType: String
        let text: String
        let attachments: [NewsResponseAttachment]?
        let postSource: PostSource
        
        enum CodingKeys: String, CodingKey {
            case id
            case ownerID = "owner_id"
            case fromID = "from_id"
            case date
            case postType = "post_type"
            case text, attachments
            case postSource = "post_source"
        }
    }
    
    struct Photos: Decodable {
        let count: Int
        let items: [Item]
        
        struct Item: Decodable {
            let id: Int
            let albumID: Int
            let ownerID: Int
            let userID: Int?
            let sizes: [NewsResponsePhotoSize]
            let text: String?
            let date: Int
            let postID: Int?
            let accessKey: String
            let comments: NewReaction
            let likes: NewReaction
            let reposts: NewReaction
            let canComment: Int
            let canRepost: Int
            
            enum CodingKeys: String, CodingKey {
                case id
                case albumID = "album_id"
                case ownerID = "owner_id"
                case userID = "user_id"
                case sizes, text, date
                case postID = "post_id"
                case accessKey = "access_key"
                case comments, likes, reposts
                case canComment = "can_comment"
                case canRepost = "can_repost"
            }
        }
    }
    
    struct PostSource: Decodable {
        let type: String
        let platform: String?
    }
    
    struct NewReaction: Decodable {
        let count: Int
    }
    
    enum CodingKeys: String, CodingKey {
        case type
        case sourceID = "source_id"
        case date
        case postID = "post_id"
        case photos
        case postType = "post_type"
        case text
        case copyHistory = "copy_history"
        case markedAsAds = "marked_as_ads"
        case attachments
        case postSource = "post_source"
        case comments, likes, reposts, views
        case isFavorite = "is_favorite"
    }
}
