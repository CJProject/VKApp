//
//  NewsResponseProfile.swift
//  VKApp
//
//  Created by Alexey Sigay on 14/09/2019.
//  Copyright © 2019 CJProject. All rights reserved.
//

import Foundation

/**
 A part of structure NewsResponse

 It includes data about new source as a human
 */

struct NewsResponseProfile: Decodable {
    let id: Int
    let firstName: String
    let lastName: String
    let isClosed: Bool?
    let canAccessClosed: Bool?
    let sex: Int
    let screenName: String?
    let photo50: String
    let photo100: String
    let online: Int
    
    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case lastName = "last_name"
        case isClosed = "is_closed"
        case canAccessClosed = "can_access_closed"
        case sex
        case screenName = "screen_name"
        case photo50 = "photo_50"
        case photo100 = "photo_100"
        case online
    }
}
