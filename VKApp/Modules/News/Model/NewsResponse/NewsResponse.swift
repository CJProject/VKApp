//
//  NewsResponse.swift
//  VKApp
//
//  Created by Alexey Sigay on 25/06/2019.
//  Copyright © 2019 CJProject. All rights reserved.
//

import Foundation

/**
 Data struct for get News
 
 The following new types are supported:
    * post;
    * photo;
    * wall_photo.
 
 [Documentation vk.com](https://vk.com/dev/newsfeed.get)
 */

struct NewsResponse: Decodable {
    var response: Response?
    let error: Error?
}

extension NewsResponse {
    struct Response: Decodable {
        var items: [NewsResponseItem]
        let profiles: [NewsResponseProfile]
        let groups: [NewsResponseGroup]
        let nextFrom: String?
        
        enum CodingKeys: String, CodingKey {
            case items, profiles, groups
            case nextFrom = "next_from"
        }
    }
    
    struct Error: Decodable {
        let errorCode: Int
        let errorMsg: String
        
        enum CodingKeys: String, CodingKey {
            case errorCode = "error_code"
            case errorMsg = "error_msg"
        }
    }
}
