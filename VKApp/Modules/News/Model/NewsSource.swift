//
//  NewsSource.swift
//  VKApp
//
//  Created by Alexey Sigay on 12.12.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import Foundation

/// Data source for NewsSourceView displaying

struct NewsSource: Hashable, Codable {
    let imageURL: String
    let name: String
    let date: String
}

extension NewsSource {
    struct Stub {
        static let data = NewsSource(
            imageURL: "mountain",
            name: "New sourse title",
            date: "10.10.1010"
        )
    }
}
