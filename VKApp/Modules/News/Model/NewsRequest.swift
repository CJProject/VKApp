//
//  NewsRequest.swift
//  VKApp
//
//  Created by Alexey Sigay on 30.11.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import Foundation

/// Request of data needed to display the news list for the current user.

struct NewsRequest: NetworkRequest {
    let filters = "post"
    let count = 100
    
    var path = "/method/newsfeed.get"
    var parameters: URLParameters? {
        return [
            "filters": filters,
            "count": count
        ]
    }
}
