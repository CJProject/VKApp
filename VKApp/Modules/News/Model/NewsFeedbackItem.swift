//
//  NewsFeedbackItem.swift
//  VKApp
//
//  Created by Alexey Sigay on 12.12.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import Foundation

/// Data source for NewsFeedbackItemView displaying

struct NewsFeedbackItem: Hashable, Codable {
    let imageName: String
    let value: Int
}

extension NewsFeedbackItem {
    struct Stub {
        static let data = NewsFeedbackItem(
            imageName: "mountain",
            value: 1_000
        )
    }
}
