//
//  NewsPhotoItem.swift
//  VKApp
//
//  Created by Alexey Sigay on 16.12.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import Foundation

/// New of photo type

struct NewsPhotoItem: Codable, Identifiable, Hashable {
    let id: Int
}
