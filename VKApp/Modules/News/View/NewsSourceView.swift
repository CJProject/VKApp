//
//  NewsSourceView.swift
//  VKApp
//
//  Created by Alexey Sigay on 11.12.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import SwiftUI

/// The view display data of a new source like photo and name of user or group and publication date

struct NewsSourceView: View {
    
    // MARK: - Properties
    
    let data: NewsSource
    let delegate: AdvancedAsyncImageDelegate?
    let imageSize = CGSize(width: 50, height: 50)
    
    // MARK: - View body
    
    var body: some View {
        HStack {
            HStack(spacing: 12) {
                AdvancedAsyncImage(
                    url: URL(string: data.imageURL),
                    delegate: delegate
                ) { image in
                    image
                }
                .frame(width: imageSize.width, height: imageSize.height)
                .cornerRadius(12)
                
                VStack(alignment: .leading, spacing: 8) {
                    Text(data.name)
                    Text(data.date)
                }
            }
            Spacer()
        }
    }
}

struct NewsSourceView_Previews: PreviewProvider {
    static var previews: some View {
        NewsSourceView(
            data: NewsSource.Stub.data,
            delegate: nil
        )
        .previewLayout(.sizeThatFits)
        
        NewsSourceView(
            data: NewsSource.Stub.data,
            delegate: nil
        )
        .previewLayout(.sizeThatFits)
        .preferredColorScheme(.dark)
        .previewDisplayName("Dark mode")
        
        NewsSourceView(
            data: NewsSource.Stub.data,
            delegate: nil
        )
        .redacted(reason: .placeholder)
        .previewLayout(.sizeThatFits)
        .previewDisplayName("Placeholder")
    }
}
