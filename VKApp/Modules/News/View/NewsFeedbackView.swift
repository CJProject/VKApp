//
//  NewsFeedbackView.swift
//  VKApp
//
//  Created by Alexey Sigay on 11.12.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import SwiftUI

/// The view displays reactions for a new
///
/// Displayed objects:
/// - likes
/// - comments
/// - reposts
/// - views

struct NewsFeedbackView: View {
    
    // MARK: - Properties
    
    let data: NewsFeedback
    
    // MARK: - View body
    
    var body: some View {
        HStack {
            HStack(spacing: 12) {
                NewsFeedbackItemView(
                    data: data.like
                )
                NewsFeedbackItemView(
                    data: data.comment
                )
                NewsFeedbackItemView(
                    data: data.repost
                )
            }
            Spacer()
            NewsFeedbackItemView(
                data: data.view
            )
        }
    }
}

struct NewsFeedbackView_Previews: PreviewProvider {
    static var previews: some View {
        NewsFeedbackView(data: NewsFeedback.Stub.data)
    }
}
