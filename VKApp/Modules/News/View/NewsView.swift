//
//  NewsView.swift
//  VKApp
//
//  Created by Alexey Sigay on 12.12.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import SwiftUI

/// A list of news of all types

struct NewsView<ViewModel: NewsViewModelProtocol & ErrorViewDelegate & AdvancedAsyncImageDelegate>: View {
    
    // MARK: - Properties
    
    @ObservedObject var viewModel: ViewModel
    
    // MARK: - View body
    
    var body: some View {
        Group {
            switch viewModel.state {
            case .none:
                ProgressView()
            case .loading:
                ProgressView()
            case .success(let newsItems):
                List(newsItems) { item in
                    newsItemView(item: item, delegate: viewModel)
                }
                .listStyle(InsetListStyle())
            case .error(let error):
                ErrorView(text: error.description, delegate: viewModel)
            }
        }.task { await viewModel.loadData() }
    }
    
    // MARK: - Private functions
    
    private func newsItemView(item: NewsItem, delegate: AdvancedAsyncImageDelegate?) -> some View {
        Group {
            switch item {
            case .post(let item):
                NewsPostItemView(
                    data: item,
                    delegate: delegate
                )
            case .photo(_):
                Divider()
            case .wallPhoto(_):
                Divider()
            }
        }
    }
}
