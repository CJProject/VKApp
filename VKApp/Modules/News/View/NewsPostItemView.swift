//
//  NewsPostItemView.swift
//  VKApp
//
//  Created by Alexey Sigay on 12.12.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import SwiftUI

/// The view displays a new of post type

struct NewsPostItemView: View {
    
    // MARK: - Properties
    
    let data: NewsPostItem
    let delegate: AdvancedAsyncImageDelegate?
    
    // MARK: - View body
    
    var body: some View {
        VStack {
            HStack {
                Spacer(minLength: 20)
                VStack(alignment: .leading) {
                    NewsSourceView(data: data.source, delegate: delegate)
                    Spacer(minLength: 4)
                    Text(data.text)
                }
                Spacer(minLength: 20)
            }

            Spacer(minLength: 8)
            HStack {
                AdvancedAsyncImage(
                    url: URL(string: data.imageURL),
                    delegate: delegate
                ) { image in
                    image
                }
                .frame(width: data.imageSize.width, height: data.imageSize.height)
            }
            Spacer(minLength: 8)

            HStack {
                Spacer(minLength: 20)
                NewsFeedbackView(data: data.feedback)
                Spacer(minLength: 20)
            }
            Spacer(minLength: 8)
        }
        .scaledToFit()
    }
}

struct NewsPostItemView_Previews: PreviewProvider {
    static var previews: some View {
        NewsPostItemView(data: NewsPostItem.Stub.data, delegate: nil)
    }
}
