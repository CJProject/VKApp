//
//  NewsFeedbackItemView.swift
//  VKApp
//
//  Created by Alexey Sigay on 12.12.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import SwiftUI

/// Item of FeedbackView
///
/// It displays an image of a reaction and a count of the reactions

struct NewsFeedbackItemView: View {
    
    // MARK: - Properties
    
    let data: NewsFeedbackItem
    
    // MARK: - View body
    
    var body: some View {
        HStack {
            Image(data.imageName)
                .resizable()
                .frame(width: 16, height: 16)
            Text(String(data.value))
        }
    }
}

struct NewsFeedbackItemView_Previews: PreviewProvider {
    static var previews: some View {
        NewsFeedbackItemView(data: NewsFeedbackItem.Stub.data)
    }
}
