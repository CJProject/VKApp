//
//  NewsViewModel.swift
//  VKApp
//
//  Created by Alexey Sigay on 11.12.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import SwiftUI

/// ViewModel of News feature to display a news list

protocol NewsViewModelProtocol: ObservableObject {
    /// Status of receiving the data from the network
    var state: State<[NewsItem]> { get }
    /// Send network request
    func loadData() async
}

final class NewsViewModel: NewsViewModelProtocol {
    
    // MARK: - Properties
    
    @Published private(set) var state: State<[NewsItem]> = .none
    
    // MARK: - Private properties
    
    private var dataStore: NewsDataStoring
    private let networkService: NewsFetching
    private let responseTransformer: NewsResponseTransforming
    
    // MARK: - Init
    
    init(
        dataStore: NewsDataStoring,
        networkService: NewsFetching,
        responseTransformer: NewsResponseTransforming
    ) {
        self.dataStore = dataStore
        self.networkService = networkService
        self.responseTransformer = responseTransformer
    }
    
    // MARK: - Functions
    
    func loadData() async {
        DispatchQueue.main.async {
            self.state = .loading
        }
        do {
            let serverResponse = try await networkService.fetchNews()
            guard let response = serverResponse.response else {
                state = .error(error: .serverError)
                return
            }
            
            var filtredResponse = response
            filtredResponse.items = response.items.filter { $0.attachments?.first?.type == "photo" }
            dataStore.response = filtredResponse
            
            let items = filtredResponse.items.compactMap { responseTransformer.transform($0) }
            DispatchQueue.main.async {
                self.state = .success(result: items)
            }
        } catch {
            DispatchQueue.main.async {
                self.state = .error(error: .serverError)
            }
        }
    }
}

extension NewsViewModel: ErrorViewDelegate {
    var repeatableOptions: ErrorViewRepeatableOptions { .repeatable }
    
    func cancelAction() {
        state = .none
    }
    
    func repeatAction() {
        Task { await loadData() }
    }
}

extension NewsViewModel: AdvancedAsyncImageDelegate {
    func imageLoadingError(_ error: Error, withURL url: URL?) {
        NSLog("\(error.localizedDescription); url: \(url?.description ?? "nil")")
    }
}
