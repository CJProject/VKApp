//
//  NewsDataStore.swift
//  VKApp
//
//  Created by Alexey Sigay on 16.12.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import Foundation

/// DataStore for News feature
protocol NewsDataStoring {
    /// Response to News network sevice request
    var response: NewsResponse.Response? { get set }
}

final class NewsDataStore: NewsDataStoring {
    var response: NewsResponse.Response? = nil
}
