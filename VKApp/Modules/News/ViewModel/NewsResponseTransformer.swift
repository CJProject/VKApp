//
//  NewsResponseTransformer.swift
//  VKApp
//
//  Created by Alexey Sigay on 13.12.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import UIKit

/// Transformer news items from network senvice repsonse to structure for display
protocol NewsResponseTransforming {
    /// Transform news item
    func transform(_ item: NewsResponseItem) -> NewsItem?
}

struct NewsResponseTransformer: NewsResponseTransforming {
    
    // MARK: - Properties
    
    let dateConverter: DateConvering
    let dataStore: NewsDataStoring
    
    // MARK: - Init
    
    init(dateConverter: DateConvering, dataStore: NewsDataStoring) {
        self.dateConverter = dateConverter
        self.dataStore = dataStore
    }
    
    // MARK: - Functions
    
    func transform(_ item: NewsResponseItem) -> NewsItem? {
        if item.type == NewsItemType.post.rawValue {
            return transformPost(item)
        } else if item.type == NewsItemType.photo.rawValue {
            return transformPhoto(item)
        } else if item.type == NewsItemType.wallPhoto.rawValue {
            return transformWallPhoto(item)
        } else {
            return nil
        }
    }
    
    // MARK: - Private functions
    
    private func transformPost(_ item: NewsResponseItem) -> NewsItem? {
        guard
            let source = transformSource(for: item),
            item.attachments?.first?.type == NewsResponseAttachmentType.photo.rawValue,
            let photo = item.attachments?.first?.photo?.sizes.last else { return nil }

        let imageWidth = UIScreen.main.bounds.width
        let imageHeight = CGFloat(photo.height) * imageWidth / CGFloat(photo.width)
        
        return .post(item: NewsPostItem(
            id: item.postID,
            source: source,
            text: item.text ?? "",
            imageURL: photo.url,
            imageSize: CGSize(width: imageWidth, height: imageHeight),
            feedback: transformFeedback(for: item)
        ))
    }
    
    private func transformPhoto(_ item: NewsResponseItem) -> NewsItem? { return nil }
    
    private func transformWallPhoto(_ item: NewsResponseItem) -> NewsItem? { return nil }
    
    
    private func transformSource(for item: NewsResponseItem) -> NewsSource? {
        var sourceName = ""
        var sourceImagePath: String?
        if item.sourceID > 0 {
            let profile = dataStore.response?.profiles.first(where: { $0.id == item.sourceID})
            sourceName = "\(profile?.firstName ?? "") \(profile?.lastName ?? "")"
            sourceImagePath = profile?.photo50
        } else {
            let group = dataStore.response?.groups.first(where: { $0.id == -item.sourceID})
            sourceName = group?.name ?? ""
            sourceImagePath = group?.photo50
        }

        guard
            !sourceName.isEmpty,
            let url = sourceImagePath else { return nil }
        
        let date = dateConverter.formatedDate(from: item.date)
        
        return NewsSource(
            imageURL: url,
            name: sourceName,
            date: date
        )
    }
    
    private func transformFeedback(for item: NewsResponseItem) -> NewsFeedback {
        return NewsFeedback(
            like: NewsFeedbackItem(imageName: "imgLike", value: item.likes?.count ?? 0),
            comment: NewsFeedbackItem(imageName: "imgComment", value: item.comments?.count ?? 0),
            repost: NewsFeedbackItem(imageName: "imgRepost", value: item.reposts?.count ?? 0),
            view: NewsFeedbackItem(imageName: "imgView", value: item.views?.count ?? 0)
        )
    }
}
