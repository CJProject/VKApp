//
//  NewsFetching.swift
//  VKApp
//
//  Created by Alexey Sigay on 09.12.2022.
//  Copyright © 2022 CJProject. All rights reserved.
//

import Foundation

/// Returns the data needed to display News list for the current user.
protocol NewsFetching {
    /// Get NewsResponse
    func fetchNews() async throws -> NewsResponse
}

extension NetworkService: NewsFetching {
    func fetchNews() async throws -> NewsResponse {
        guard let request = NetworkService.requestFactory.create(NewsRequest()) else {
            throw NetworkError.invalidRequest
        }

        return try await NetworkService.shared.fetchData(on: request)
    }
}
